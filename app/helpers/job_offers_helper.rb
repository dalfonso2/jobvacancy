# Helper methods defined here can be accessed in any controller or view in the application

module JobOffersHelper
  TITLE_ERROR = 'Title is mandatory'.freeze
  REQUIRED_XP_ERROR = 'Required experience is mandatory and should be a non-negative integer'.freeze
  def job_offer_params
    params[:job_offer].to_h.symbolize_keys
  end

  def flash_error(errors)
    flash.now[:error] = errors.key?(:title) ? TITLE_ERROR : REQUIRED_XP_ERROR
  end

  def decorate_required_experience(required_experience)
    required_experience.zero? ? 'Not specified' : required_experience
  end
end

JobVacancy::App.helpers JobOffersHelper
