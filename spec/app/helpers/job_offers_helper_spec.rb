require 'spec_helper'

describe 'JobOffersHelper' do
  let(:class_being_tested) do
    class JobOffersHelperClass
      include JobOffersHelper
    end
  end

  it('decorate 1 returns 1') do
    expect(class_being_tested.new.decorate_required_experience(1)).to eq(1)
  end

  it('decorate 0 returns "Not specified"') do
    expect(class_being_tested.new.decorate_required_experience(0)).to eq('Not specified')
  end
end
