Feature: Job Offers - Experience required
  In order to only get applications that match my experience requirements
  As a job offerer
  I want to specify a minimum amount of years of experience required

  Background:
  Given I am logged in as job offerer

  Scenario: Create new offer with required experience
    Given I access the new offer page
    When I fill the title with "Programmer needed - 10 yrs XP"
    And I ask for 10 years of experience
		And confirm the new offer    
    Then I should see "Offer created"
    And I should see "Programmer needed - 10 yrs XP" in My Offers

  Scenario: Required experience is a non negative integer
    Given I access the new offer page
    When I fill the title with "Programmer needed - No XP required"
    And I ask for -1 years of experience
		And confirm the new offer    
    Then I should see an error related to "Required experience"
    And I should not see "Programmer needed - No XP required" in My Offers

  Scenario: Required experience is shown in offers list
    Given There is an offer with title "Programmer vacancy" and 9 yrs of required experience
    When I access Job Offers page
    Then I should see "Programmer vacancy"
    And I should see "9" under "Required experience" corresponding to "Programmer vacancy"

  Scenario: When experience is 0, it should be shown as "Not specified"
    Given There is an offer with title "Programmer vacancy" and 0 yrs of required experience
    When I access Job Offers page
    Then I should see "Programmer vacancy"
    And I should see "Not specified" under "Required experience" corresponding to "Programmer vacancy"
