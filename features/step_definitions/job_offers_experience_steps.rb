When('I ask for {int} years of experience') do |required_experience|
  fill_in('job_offer[required_experience]', with: required_experience)
end

Then('I should see an error related to {string}') do |content|
  flash_tag = page.find('div#flashMessage.alert-error')
  flash_tag.should have_content(content)
end

Given('There is an offer with title {string} and {int} yrs of required experience') do |offer_title, xp|
  JobOfferRepository.new.delete_all

  visit '/job_offers/new'
  fill_in('job_offer[title]', with: offer_title)
  fill_in('job_offer[required_experience]', with: xp)
  click_button('Create')

  visit '/job_offers/my'
  click_button('Activate')
end

When('I access Job Offers page') do
  visit '/job_offers/latest'
end

Then('I should see {string} under {string} corresponding to {string}') do |expected, col_title, row_text|
  column_zero_based = page.all('th').map(&:text).index(col_title)
  capybara_row = page.find(:css, 'td', text: row_text).find(:xpath, './parent::tr')
  cell_text = capybara_row.find(:css, ":nth-child(#{column_zero_based + 1})").text
  cell_text.should eq(expected)
end
